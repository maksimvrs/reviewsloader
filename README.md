# Utility for downloading app reviews in the App Store.
# Run
```bash
python3 -m Reviews 1065803457
```
# Help
```bash
python3 -m Reviews --help
```
# Install
```bash
pip3 install ReviewsLoader
```