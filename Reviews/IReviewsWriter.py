class IRewiewsWriter(object):
    """
    Write data to storage.
    """
    async def write(self, data: dict):
        """
        Write one row.
        """
        raise NotImplementedError
    
    def close(self):
        raise NotImplementedError
