class IDataConverter(object):
    """
    Data conversion that returns the server to the export format.
    """
    @staticmethod
    def get_fieldnames():
        """
        Field header names.
        """
        raise NotImplementedError

    @staticmethod
    def convert(data: dict) -> dict:
        raise NotImplementedError
